<?php

namespace Example;

use Stiply\Contracts\Auth\TokenRepository as ITokenRepository;

class TokenRepository implements ITokenRepository
{
    private string $token;

    public function __construct(string $token)
    {
        $this->token = $token;    
    }

    public function token(): string {
        return $this->token;
    }
}