# stiply-php-sdk-example

Example project for the Stiply PHP SDK.
- [Stiply PHP SDK on Gitlab](https://gitlab.com/4cee/stiply/stiply-php-sdk)
- [Stiply PHP SDK on Packagist](https://packagist.org/packages/stiply/stiply-php-sdk)
- [API Documentation](https://www.stiply.nl/api)

## Requirements
- [PHP](https://www.php.net/) >= 7.3.0

## Installation
Clone this example:
```shell
git clone git@gitlab.com:4cee/stiply/stiply-php-sdk-example.git

cd stiply-php-sdk-example
```

Run [Composer](https://getcomposer.org/) install:

```shell
composer install
```

## Usage
- Copy the .env.example file to .env Fill in the variables in this file
    - Go to the [Stiply API page](https://app.stiply.nl/groups/api) and create a new Personal Access Token
    - Add the personal access token to the .env file in variable PAT=
    - add your email address to the .env file in variable SIGNER_EMAIL= 
- run the submit-signrequest script

```shell
php ./submit-signrequest.php
```

A new sign request is created through the API.